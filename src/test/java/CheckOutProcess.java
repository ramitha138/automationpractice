import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CheckOutProcess extends InitiateClass {
    public String username;
    public String password;
    public String customerName;

    //Create objects
    SignIn objSignIn;
    InitiateClass objInitiateClass;
    AddItemsFromHomePage objAddItemsFromHomePage;
    Checkout objCheckout;

    //Get login information from the Data.Properties file before execute the test
    @BeforeTest()
    public void LoadData(){
        Properties properties = new Properties();
        try {
            String filePath = System.getProperty("user.dir");
            properties.load(new FileInputStream(filePath + "\\util\\Data.properties"));

            customerName = properties.getProperty("CustomerName");
            username = properties.getProperty("UserName");
            password = properties.getProperty("Password");


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 1)
    public void InitiateTheDriver(){
        driver = InitiateClass.driverInstance();
    }

    @Test(priority = 2)
    public void LoginToTheSystem(){
        objSignIn = new SignIn();
        objSignIn.SignInButton();
        objSignIn.LoginMethod(username,password);
    }

    @Test(priority = 3)
    public void VerifyLoginHasSuccessful(){
        objSignIn.VerifyLoginIsSuccessful(customerName);
    }

    @Test(priority = 4)
    public void HomePageLoad(){
        objInitiateClass = new InitiateClass();
        objInitiateClass.GoToHomePage();
    }
    @Test(priority = 5)
    public void AddWomanItems(){
        objAddItemsFromHomePage = new AddItemsFromHomePage();
        //Please select item count by entering a number between 1 to 7
        objAddItemsFromHomePage.AddItems(7);

    }
    @Test(priority = 6)
    public void checkOutShoppingCart(){
        objCheckout = new Checkout();
        objCheckout.CheckoutShoppingCart();
        objCheckout.ProceedToCheckOut();
        //Please enter number 1 for pay by bank wire and number 2 for pay by cheque
        objCheckout.SelectPaymentMethod(1);
        objCheckout.ConfirmOrder();
    }

}
