import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class InitiateClass {

    //Logo
    By companyLogo = By.xpath("//a[@title='My Store']");

    public static WebDriver driver;
    public static String URL;
    public static String UserName;
    public static String Password;

    // This method initiate the driver
    public static WebDriver driverInstance() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(".\\util\\Data.properties"));
            URL = properties.getProperty("URL");
            UserName = properties.getProperty("UserName");
            Password = properties.getProperty("Password");


        } catch (IOException e) {
            e.printStackTrace();
        }

        String filePath = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", filePath + "\\webDrivers\\chromedriver.exe");

        driver = new ChromeDriver();
        Reporter.log("Browser opened");
        driver.manage().window().maximize();
        Reporter.log("Browser Maximized");
        //Wait
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.get(URL);
        //wait
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

        return driver;
    }

    //Go to the home page companyLogo
    public void GoToHomePage(){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);

        // click on the logo and go to the home page
        WebElement companyLogoElement = driver.findElement(companyLogo);
        wait.until(ExpectedConditions.elementToBeClickable(companyLogoElement));
        companyLogoElement.click();

        //Verify home page is loaded
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url,URL);

    }
}
