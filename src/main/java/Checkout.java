import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.concurrent.TimeUnit;

public class Checkout extends InitiateClass {
    By cartIcon = By.xpath("//a[@title='View my shopping cart']");
    By checkoutButton = By.id("button_order_cart");

    By proceedToCheckoutButton = By.xpath("//a[@title='Proceed to checkout']//*[text()='Proceed to checkout']");
    By processAddressButton = By.xpath("//button[@name='processAddress']");
    By termsConditionsCheckBox = By.id("cgv");
    By processCarrierButton = By.xpath("//button[@name='processCarrier']");

    //Payment method
    By bankWire = By.className("bankwire");
    By cheque = By.className("cheque");

    // I confirm my order button
    By confirmOrderButton = By.xpath("//button[@type='submit']//*[text()='I confirm my order']");

    public void CheckoutShoppingCart(){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);
        // Hover mouse on the shopping cart
        WebElement cartIconElement = driver.findElement(cartIcon);
        wait.until(ExpectedConditions.presenceOfElementLocated(cartIcon));
        action.moveToElement(cartIconElement).build().perform();

        //click on the checkout button
        WebElement checkoutButtonElement = driver.findElement(checkoutButton);
        wait.until(ExpectedConditions.elementToBeClickable(checkoutButtonElement));
        action.moveToElement(checkoutButtonElement).click().build().perform();
    }

    public void ProceedToCheckOut(){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        WebElement proceedToCheckoutButtonElement = driver.findElement(proceedToCheckoutButton);
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButtonElement));
        action.moveToElement(proceedToCheckoutButtonElement).click().build().perform();

        //Click on the proceed to check out in address details page
        WebElement processAddressButtonElement = driver.findElement(processAddressButton);
        wait.until(ExpectedConditions.elementToBeClickable(processAddressButtonElement));
        action.moveToElement(processAddressButtonElement).click().build().perform();

        // Tick on the terms Conditions CheckBox
        WebElement termsConditionsCheckBoxElement = driver.findElement(termsConditionsCheckBox);
        wait.until(ExpectedConditions.presenceOfElementLocated(termsConditionsCheckBox));
        action.moveToElement(termsConditionsCheckBoxElement).click().build().perform();

        //click on the process Carrier Button
        WebElement processCarrierButtonElement = driver.findElement(processCarrierButton);
        wait.until(ExpectedConditions.elementToBeClickable(processCarrierButtonElement));
        action.moveToElement(processCarrierButtonElement).click().build().perform();

    }

    public void SelectPaymentMethod(int paymentType){

       //int payType=paymentType;
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        WebElement bankWireElement = driver.findElement(bankWire);
        WebElement chequeElement = driver.findElement(cheque);

        wait.until(ExpectedConditions.elementToBeClickable(bankWireElement));
        wait.until(ExpectedConditions.elementToBeClickable(chequeElement));

        if(paymentType == 1){
            action.moveToElement(bankWireElement).click().build().perform();
        }else if(paymentType ==2 ){
            action.moveToElement(chequeElement).click().build().perform();
        }else {
            Reporter.log("Incorrect payment type selected, please enter number 1 or 2");
        }
    }

    //confirm order
    public void ConfirmOrder(){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        WebElement confirmOrderButtonElement = driver.findElement(confirmOrderButton);
        wait.until(ExpectedConditions.elementToBeClickable(confirmOrderButtonElement));
        action.moveToElement(confirmOrderButtonElement).click().build().perform();

    }
}
