import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class AddItemsFromHomePage extends InitiateClass {

    By womenTab = By.xpath("//a[@title='Women']");
    By listView = By.xpath("//i[@class='icon-th-list']");
    By okIcon = By.xpath("//i[@class='icon-ok']");


    //Validate user name
    public void AddItems(int ItemCount){


        int itemCount = ItemCount;

        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        // click on the woman tab and go to woman page
        WebElement womenTb = driver.findElement(womenTab);
        wait.until(ExpectedConditions.elementToBeClickable(womenTb));
        action.moveToElement(womenTb).click().build().perform();

        //list view icon click
        WebElement listViewElement = driver.findElement(listView);
        wait.until(ExpectedConditions.elementToBeClickable(listViewElement));
        action.moveToElement(listViewElement).click().build().perform();

        for(int i=1;i<=itemCount;i++){

            By addToCartButton = By.xpath("//a[@data-id-product='"+i+"']//*[text()='Add to cart']");
            By continueShoppingButon = By.xpath("//span[@title='Continue shopping']");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Add to cart
            WebElement addToCartBtnElement = driver.findElement(addToCartButton);
            wait.until(ExpectedConditions.elementToBeClickable(addToCartBtnElement));
            action.moveToElement(addToCartBtnElement).click().build().perform();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //verify ok icon is displayed
            WebElement okIconElement = driver.findElement(okIcon);
            wait.until(ExpectedConditions.presenceOfElementLocated(okIcon));
            okIconElement.isDisplayed();

            // click on the continue shopping button
            WebElement continueShoppingButtonElement = driver.findElement(continueShoppingButon);
            wait.until(ExpectedConditions.presenceOfElementLocated(continueShoppingButon));
            action.moveToElement(continueShoppingButtonElement).click().build().perform();

        }



    }
}
