import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class SignIn extends InitiateClass {
    //SignIn
    By HomeSignInButton = By.className("login");
    By UsernameTextBox = By.id("email");
    By PasswordTextBox = By.id("passwd");
    By SiginInButton = By.id("SubmitLogin");

    //customer name on the home screen
    By customerName = By.className("account");

    //Click on the sign in button
    public void SignInButton(){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        //Click on the signIn Button
        WebElement signInBtn = driver.findElement(HomeSignInButton);
        wait.until(ExpectedConditions.elementToBeClickable(signInBtn));
        signInBtn.click();
    }

    //Login to the system using Login Details, Login details can be obtain from the Data.properties file
    public void LoginMethod(String username, String Password){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);



        //Click and send email address to the Email Address Textbox
        WebElement usernameTxtBox = driver.findElement(UsernameTextBox);
        wait.until(ExpectedConditions.elementToBeClickable(usernameTxtBox));
        action.moveToElement(usernameTxtBox).click().sendKeys(username).build().perform();

        //Click and send password to the password Address Textbox
        WebElement passwordTxtBox = driver.findElement(PasswordTextBox);
        wait.until(ExpectedConditions.elementToBeClickable(passwordTxtBox));
        action.moveToElement(passwordTxtBox).click().sendKeys(Password).build().perform();

        //click on the SiginInButton and login to the system with given username and password
        WebElement SiginInBtn = driver.findElement(SiginInButton);
        wait.until(ExpectedConditions.elementToBeClickable(SiginInBtn));
        SiginInBtn.click();

    }

    //Validate user name on the home page
    public void VerifyLoginIsSuccessful(String custName){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        //Soft assert continue the test execution without terminate
        SoftAssert softAssert = new SoftAssert();

        //Get customer name from home screen and validate
        WebElement customerNameElement = driver.findElement(customerName);
        wait.until(ExpectedConditions.elementToBeClickable(customerNameElement));
        Assert.assertEquals(customerNameElement.getText(),custName);

    }
}
