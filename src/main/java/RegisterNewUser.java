import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class RegisterNewUser extends InitiateClass {

    By emailTextField = By.id("email_create");
    By createAnAccountButton = By.id("SubmitCreate");

    public void EnterEmailAddress(String email){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.pollingEvery(2, TimeUnit.SECONDS);
        Actions action = new Actions(driver);

        //Enter email address
        WebElement emailTextFieldElement = driver.findElement(emailTextField);
        wait.until(ExpectedConditions.elementToBeClickable(emailTextFieldElement));
        action.moveToElement(emailTextFieldElement).sendKeys(email).build().perform();
    }

}
